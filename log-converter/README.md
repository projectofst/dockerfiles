# LogConverter

![Sample](https://i.ibb.co/562xdS1/Screen-Shot-2021-04-30-at-00-26-11.png)

This is a docker image meant to make it easier to convert logs comming from kvaser to .mat files using fcp-data-muncher. This tool is ten times faster than the software given by kvaser (a diference of about 1h to 5min).

It was created due to the limitations of the fop-data-muncher that make it very hard to install and use. An added bonus is the fact that using this docker image we can run this tool in any OS and get the same results.

Por more information on the fcp-data-muncher and fop itself go check the appropriate repositories.

## How to use

**You don't need any of the files in this repo to run... no need to download/clone this repo!!!**

To convert the CSV's from kvaser logger to MAT file you need to first have a folder in your computer with all the CSV's you want to convert and a JSON file describing the CAN signals (created using fcp).

1. Create folder with all the CSV's and a JSON file & find the folder path (eg. ~/Documents/teste1 or c://Users/FST/teste1)

2. Be sure you have docker installed and running

3. Run the following command on a terminal (Windows, Linux or macOS) replacing the <path_here> with the folder path described in step (1)

   Command:

   ```bash
   docker run -i -v <path_here>:/logs fstlx/log-converter:latest
   ```

   Example:

   ```bash
   docker run -i -v ~/Documents/FST/bombarral_test:/logs fstlx/log-converter:latest
   ```

   The docker image will start and run the tool for every CSV in the given folder and create the MAT files. You will be prompt to confirm a few things.

All done! Enjoy you MAT files.
