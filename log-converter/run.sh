#!/bin/bash

# Introduction and making sure the user knows what he/she is doing
echo
echo "       dMP    .aMMMb  .aMMMMP .aMMMb  .aMMMb  dMMMMb  dMP dMP dMMMMMP dMMMMb dMMMMMMP dMMMMMP dMMMMb "
echo "      dMP    dMP\"dMP dMP\"    dMP\"VMP dMP\"dMP dMP dMP dMP dMP dMP     dMP.dMP   dMP   dMP     dMP.dMP "
echo "     dMP    dMP dMP dMP MMP\"dMP     dMP dMP dMP dMP dMP dMP dMMMP   dMMMMK\"   dMP   dMMMP   dMMMMK\"  "
echo "    dMP    dMP.aMP dMP.dMP dMP.aMP dMP.aMP dMP dMP  YMvAP\" dMP     dMP\"AMF   dMP   dMP     dMP\"AMF   "
echo "   dMMMMMP VMMMP\"  VMMMP\"  VMMMP\"  VMMMP\" dMP dMP    VP\"  dMMMMMP dMP dMP   dMP   dMMMMMP dMP dMP    "
echo

echo "    Welcome!!!! This tool will convert your logs"
echo 
echo "  I hope you have run me using a command similar to:"
echo "       docker run -i -v ~/Documents/FST/logs_test1:/logs fstlx/log-converter:latest"
echo "         --> Replace '~/Doc...' with your folder path"
echo "  if not be gone..."
echo
echo " Does your folder containe both the CSV's and a JSON file?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) echo "Nice :)"; break;;
        No ) echo "You suck" && exit;;
    esac
done

echo
echo " It is essential that you give (mount) the folder with the CSV's to the docker (see example above)." 
echo " Did you mount your folder?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) echo "Nice :)"; break;;
        No ) echo "You piece of garbage" && exit;;
    esac
done

# Touch metadata file
echo
echo "Let the fun begin..."
touch /logs/metadata
echo "> metadata file created or checked..."

# Finding JSON file
json=$(find /logs -maxdepth 1 | grep .json)
if [ -z "$json" ]
then
    echo "There is no JSON in the folder provided..."
    echo "GIVE ME THE JSON NEXT TIME!!! It should be in the provided folder along with the CSV's"
    exit
else
    echo "> JSON file detected..."
fi

# Starting the file conversion
foldr="/logs"
echo
echo "Lets start the conversion!!!"
echo "Be patient..."

for l in  $(find $foldr | grep .csv)
do 
    fcp-data-muncher --quiet -i kvaser_csv -o mat $json $l ${l%.csv}.mat $foldr/metadata
    echo "> New MAT file created and converted"
done
echo
echo "Conversions done!!!"


