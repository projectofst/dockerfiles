# DockerFiles

This repo contains all the docker files for the containers the team uses. Also the CI/CD is built to build, teste and deploy the containers to docker hub.

## Images

The different images we have currently are:

- [Qt5](qt5/) 
  - ubuntu19 (for both amd64 and ARM)
  - arch
  - win64 (windows)
- [xc16](xc16)