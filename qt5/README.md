# qt5 Docker Image

[![website badge](https://img.shields.io/badge/website-fstlisboa.com-blue?style=flat-square)](https://fstlisboa.com/) [![gitlab](https://img.shields.io/badge/GitLab-orange?style=flat-square&logo=gitlab)](https://gitlab.com/projectofst/dockerfiles)

**Multi-architecture** (x86 and ARM), **multi-distro** (Ubuntu, Arch...), **multi-OS** (windows and linux), **Qt5** images.

These dockers were created to build, test and deploy qt5 applications. As a Formula Student team we use them in our CI/CD to build our proprietary software, do some tests and deploy (using [linuxdeployqt](https://github.com/probonopd/linuxdeployqt) for example). They work well on Gitlab CI/CD.



## Tags

The different versions are identified by the respective tags. They can serve different purposes. For almost every tag there is an equivalent tag that finishes in "_testing". This is because every time we update an image we first test it and if it fails our tests it won't be pushed to the respective tag. This gives us time to test with the convenience of the image being on docker hub.

|    Docker Tag    |           OS            |   Qt   |    Architectures     |                           Packages                           |
| :--------------: | :---------------------: | :----: | :------------------: | :----------------------------------------------------------: |
|     ubuntu18     |      Ubuntu 18.04       | 5.15.2 |        amd64         | qt515base (Beineri repository), qt515charts-no-lgpl, qt515declarative, qt515quickcontrols2, qt515serialport, qt515serialbusqtcreator, |
| latest, ubuntu19 |      Ubuntu 19.10       | 5.12.4 | amd64, arm/v7, arm64 | qt5-default, libqt5charts5-dev, qtdeclarative5-dev, qtquickcontrols2-5-dev, libqt5serialport5-dev, libqt5serialbus5-dev, protobuf-compiler, yaml-cpp |
|       arch       |          Arch           | 5.15.0 |        amd64         | qtcreator, qt5-charts, qt5-serialport, qt5-serialbus, qt5-base, qt5-translations, protobuf |
|      win64       | windows servercore:1809 | 5.15.0 |        amd64         | win64_msvc2017_64, win32_msvc2017, qtbase, qtdeclarative, qttools, qtscript |

### Use latest version

This is the simples. Since it suns on multiple architecture you don't need to think about anything. Just run:

```
docker run -it fstlx/qt5:latest
```



## Ubuntu18 Tag

This image is based on Ubuntu 18.04. This is nice version of Ubuntu to run because it is recent enough to have a lot of updated Qt5 packages (using the Beineri repository) but not too recent. If you want the most recent possible use Arch.

We made this one because it is not that recent. That means we can deploy apps with all the libraries and they will support most versions of linux distros. Also Ubuntu containers are available for multiple architectures (both x86 and multiple ARM). This enables us to use Raspberry Pi for CI/CD jobs.

We use linuxdeployqt to easily deploy apps compatible with all Linux distros. The problem is that when deploying on arch the "glib" and other libraries are too recent so most distros will be on a older versions and therefore not be able to run the deployed app.

#### How to use

To pull the image:

```
docker pull fstlx/qt5:ubuntu18
```

or if you want to run it attached:

```
docker run -it fstlx/qt5:ubuntu18
```

## Arch Tag

Arch is a perfect distribution for this because it is light and most of the times the one with the most recent versions of bot Qt5 and the additional packages. It is always hard to find all the packages we need form a package manager on other distros but with Arch it has been a breeze.

This is the main image we use and the one we recommend unless there is a reason to use another.

#### How to use

To pull the image:

```
docker pull fstlx/qt5:arch
```

or if you want to run it attached:

```
docker run -it fstlx/qt5:arch
```
